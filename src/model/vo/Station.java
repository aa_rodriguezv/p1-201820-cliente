package model.vo;

import java.time.LocalDateTime;

public class Station implements Comparable<Station> {
	private int stationId;
	private String stationName;
	private LocalDateTime startDate;
	//TODO Completar

	public Station(int stationId, String stationName, LocalDateTime startDate) {
		this.stationId = stationId;
		this.stationName = stationName;
		this.startDate = startDate;
	}

	@Override
	public int compareTo(Station o) {
		// TODO Auto-generated method stub
		return 0;
	}

	public LocalDateTime getStartDate() {
		return startDate;
	}

	public int getStationId() {
		return stationId;
	}

	public String getStationName() {
		return stationName;
	}
}
